package com.microservice.c.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MicroServiceCController {
	
	
	@GetMapping
	public String hello() {
		return "Hello from service C";
		
	}

}
