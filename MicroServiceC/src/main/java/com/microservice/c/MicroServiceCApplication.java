package com.microservice.c;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class MicroServiceCApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroServiceCApplication.class, args);
	}

}
