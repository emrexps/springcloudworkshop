package com.microservice.b.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "TableB")
@Data
public class EntityB {

	@Id
	@GeneratedValue
	private Long id;
	private String field_b;
	
}
