package com.microservice.b.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TableARepository extends JpaRepository<EntityB, Long> {

}
