package com.microservice.b;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class MicroServiceBApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroServiceBApplication.class, args);
	}

}
