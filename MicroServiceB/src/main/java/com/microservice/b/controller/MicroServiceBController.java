package com.microservice.b.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MicroServiceBController {

	
	@GetMapping
	public String greeting() {
		return "Hello from Microservice B";
	}
	
	
}
