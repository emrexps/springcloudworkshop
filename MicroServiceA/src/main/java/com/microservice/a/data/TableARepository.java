package com.microservice.a.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TableARepository extends JpaRepository<EntityA, Long> {

}
