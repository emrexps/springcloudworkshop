package com.microservice.a.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "TableA")
@Data
public class EntityA {

	@Id
	@GeneratedValue
	private Long id;
	private String field_a;
	
}
