package com.microservice.a.controller;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.inject.internal.ErrorsException;
import com.microservice.a.config.PropertyConfig;
import com.microservice.a.dto.Greeting;
import com.microservice.a.messageproducer.MessageProducer;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
public class MicroServiceAController {

	private final PropertyConfig propertyConfig;

	private final LoadBalancerClient loadBalancer;

	private final RestTemplate restTemplate;

	private final MessageProducer messageProducer;

	public MicroServiceAController(PropertyConfig propertyConfig, LoadBalancerClient loadBalancer,
			RestTemplate restTemplate, MessageProducer messageProducer) {

		this.propertyConfig = propertyConfig;
		this.loadBalancer = loadBalancer;
		this.restTemplate = restTemplate;
		this.messageProducer = messageProducer;
	}

	@GetMapping("/publish/{msg}/{name}")
	public void publishToKafka(@PathVariable("msg") String msg, @PathVariable("name") String name) {
		Greeting greeting = new Greeting();
		greeting.setMsg(msg);
		greeting.setName(name);
		messageProducer.sendMessage(greeting);

	}

	@GetMapping("/config-value")
	public String getConfig() {
		return "Value: " + propertyConfig.getValue();
	}

	@GetMapping("/get-from-serviceb")
	public String getMessageFromServiceB() {
		ServiceInstance serviceInstance = loadBalancer.choose("MicroserviceB");
		String serviceBUrl = serviceInstance.getUri() + "/";
		return restTemplate.getForObject(serviceBUrl, String.class);
	}

	@GetMapping()
	@HystrixCommand(fallbackMethod = "fallback")
	public String success() throws ErrorsException {

		return "hello from service A";
	}

	@GetMapping("/fail")
	@HystrixCommand(fallbackMethod = "fallback")
	public String fail() throws ErrorsException {
		int a = 1 / 0;
		return "hello from service A";
	}

	public String fallback() {
		return "Service A is not available";
	}

}
