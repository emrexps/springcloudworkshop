package com.microservice.a.messageproducer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.microservice.a.dto.Greeting;

@Component
public class MessageProducer {
	
	 @Autowired
     private KafkaTemplate<String, Greeting> kafkaTemplate;
   
     @Value(value = "${message.topic.name}")
     private String topicName;

     public void sendMessage(Greeting message) {

         ListenableFuture<SendResult<String, Greeting>> future = kafkaTemplate.send(topicName, message);

         future.addCallback(new ListenableFutureCallback<SendResult<String, Greeting>>() {
             @Override
             public void onSuccess(SendResult<String, Greeting> result) {
                 System.out.println("Sent message=[" + message + "] with offset=[" + result.getRecordMetadata()
                     .offset() + "]");
             }

             @Override
             public void onFailure(Throwable ex) {
                 System.out.println("Unable to send message=[" + message + "] due to : " + ex.getMessage());
             }
         });
     }
}