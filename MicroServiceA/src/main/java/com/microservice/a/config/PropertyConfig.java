package com.microservice.a.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
public class PropertyConfig {
	
	@Value("${myvariable.property}")
    private String value;

    public String getValue() {
        return value;
    }

}